#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

void Intro()
{
    cout << "\t\t\t\t\t\t\t\t\t\tAutorzy: Igor Tokarski" << endl;
    cout << "\t\t\t\t\t\t\t\t\t\t\t Wiktor Uszko" << endl;
    cout << endl << "\t\t\t\t\tMILIONERZY" << endl;
    cout << "\t\t\t\t     wersja bez reklam" << endl;
    cout << "\n\n\n";
    cout << endl << "Nacisnij Enter aby rozpoczac gre...";
    cin.get();
    system ("CLS");
}

void ResultScreen(int stage)
{
    system ("CLS");

    cout << endl << endl << "Doszedles do " << stage << " etapu." << endl;

}

struct QuestionData
{
    int id;
    string Question;
    string A;
    string B;
    string C;
    string D;
    int Answer;
};

void ReadFile(vector<QuestionData>&vTab, string fileName)
{
    vTab.erase( vTab.begin(), vTab.end());
    QuestionData data;
    string line;
    ifstream file(fileName, ios::in);
    if(file.good() == true)
    {
        while(!file.eof())
        {
            getline(file,line, ';');
            data.id = atoi(line.c_str());

            getline(file,line, ';');
            data.Question=line;

            getline(file,line, ';');
            data.A=line;

            getline(file,line, ';');
            data.B=line;

            getline(file,line, ';');
            data.C=line;

            getline(file,line, ';');
            data.D=line;

            getline(file,line, '\n');
            data.Answer=atoi(line.c_str());
            vTab.push_back(data);
        }
    }
    else
        cout << "Nie mozna otworzyc plik" << endl;
}

int RandomQuestion(vector<QuestionData>&vTab, int stage)
{
    srand(time(NULL));
    int randomQuest = rand()%10 + 1;
    string filecsv = to_string(stage);
    string tmp = ".csv";
    string folder = "..\\project_07_35242\\questions\\";
    string forUse = folder + filecsv + tmp;


    ReadFile(vTab, forUse);

    return randomQuest;
}

void PhoneFriend(vector<QuestionData>&vTab, int random, int phone, int stage)
{
    if(phone == 1)
    {
        if(stage <= 5)
        {
            cout << "Jestem pewien, ze poprawna odpowiedzia jest odpowiedz nr. " << vTab[random].Answer << "!" << endl << endl;
        }
        else
        {
            int probablyWrong = rand()%4 + 1;
            cout << "Wydaje mi sie, ze poprawna odpowiedzia jest odpowiedz nr. " << probablyWrong << "." << endl;
        }
    }
}

struct HalfStr
{
    int One = 0;
    int Two = 0;
};

void HalfHalf(vector<QuestionData>&vTab, int random, vector<HalfStr>&vHalf)
{
    HalfStr halfrandom;
    halfrandom.One = rand()%4 + 1;
    halfrandom.Two = rand()%4 + 1;
    while(halfrandom.One == vTab[random].Answer)
    {
        halfrandom.One = rand()%4 + 1;
    }
    while(halfrandom.Two == vTab[random].Answer || halfrandom.Two == halfrandom.One)
    {
        halfrandom.Two = rand()%4 + 1;
    }
    vHalf.push_back(halfrandom);
}

void AskAudience(vector<QuestionData>&vTab, int random)
{
    srand(time(NULL));
    int vote = 100;
    int correctAns = rand()%51 + 50;
    vote -= correctAns;
    int wrongAns1 = rand()%vote + 0;
    vote -= wrongAns1;
    int wrongAns2 = rand()%vote + 0;
    vote -= wrongAns2;
    int wrongAns3 = vote;

    int One = rand()%4 + 1;
    int Two = rand()%4 + 1;
    int Three = rand()%4 + 1;
    int Four = rand()%4 + 1;

    while(One != vTab[random].Answer)
    {
        One = rand()%4 + 1;
    }

    while(Two == One)
    {
        Two = rand()%4 + 1;
    }

    while(Three == Two || Three == One)
    {
        Three = rand()%4 + 1;
    }

    while(Four == Three || Four == Two || Four == One)
    {
        Four = rand()%4 + 1;
    }

    for(int i = 1; i <= 4; i++)
    {
        if(One == i)
        {
            cout << endl << "" << vTab[random].Answer << ". " << correctAns << "%";
        }
        if(Two == i)
        {
            cout << endl << "" << i << ". " << wrongAns1 << "%";
        }
        if(Three == i)
        {
            cout << endl << "" << i << ". " << wrongAns2 << "%";
        }
        if(Four == i)
        {
            cout << endl << "" << i << ". " << wrongAns3 << "%";
        }
    }
    cout << endl;
}

void TheGame(vector<QuestionData>&vTab, int randomQuest, int stage, int phone, int half, int audience, vector<HalfStr>&vHalf)
{
    randomQuest = RandomQuestion(vTab, stage);
    int i = randomQuest;

    cout << endl << "PYTANIE " << stage << ": " << endl;
    cout << vTab[i].Question << endl << endl;
    cout << "1." << vTab[i].A << endl;
    cout << "2." << vTab[i].B << endl;
    cout << "3." << vTab[i].C << endl;
    cout << "4." << vTab[i].D;

    cout << endl << endl;

    error:
    int answer;
    cout << endl << "Dostepne kola ratunkowe:" << endl;
    if(phone == 1)
    {
        cout << "7. Telefon do przyjaciela " << endl;
    }
    if(half == 1)
    {
        cout << "8. 50 na 50 " << endl;
    }
    if(audience == 1)
    {
        cout << "9. Zapytaj publicznosci " << endl;
    }

    cout << endl << "Podaj odpowiedz (1-4) lub wybierz dostepne kolo ratunkowe (7-9): ";
    cin >> answer;

    if(answer == 7 && phone == 1)
    {
        PhoneFriend(vTab, randomQuest, phone, stage);
        phone--;
        goto error;
    }

    if(answer == 8 && half == 1)
    {
        HalfHalf(vTab, randomQuest, vHalf);
        system ("CLS");
        cout << endl << "PYTANIE " << stage << ": " << endl;
        cout << vTab[i].Question << endl << endl;
        if(vHalf[0].One != 1)
            if(vHalf[0].Two != 1)
                cout << "1." << vTab[i].A << endl;
        if(vHalf[0].One != 2)
            if(vHalf[0].Two != 2)
                cout << "2." << vTab[i].B << endl;
        if(vHalf[0].One != 3)
            if(vHalf[0].Two != 3)
                cout << "3." << vTab[i].C << endl;
        if(vHalf[0].One != 4)
            if(vHalf[0].Two != 4)
                cout << "4." << vTab[i].D << endl;
        half--;
        goto error;
    }

    if(answer == 9 && audience == 1)
    {
        AskAudience(vTab, randomQuest);
        audience--;
        goto error;
    }

    if(answer == vTab[randomQuest].Answer)
    {
        cout << endl << "Poprawna odpowiedz!" << endl;
        if(stage < 15)
        {
            cout << "Czy jestes gotow przejsc do nastepnego pytania?" << endl;
            cout << "Nacisnij enter aby kontynuowac...";
            cin.get();
            cin.get();
            system ("CLS");
            stage++;
            TheGame(vTab, randomQuest, stage, phone, half, audience, vHalf);
        }
        else
        {
            cout << endl << "DOSZEDLES DO KONCA!" << endl;
            cout << "Nacisnij enter aby przejsc do podsumowania...";
            cin.get();
            cin.get();
            ResultScreen(stage);
        }
    }

    else if(answer != vTab[randomQuest].Answer && answer <= 4 && answer >= 1)
    {
        cout << endl << "Niepoprawna odpowiedz!" << endl;
        cout << "Poprawna odpowiedz to: " << vTab[randomQuest].Answer << "!" << endl << endl;
        cout << "Nacisnij enter aby przejsc do podsumowania...";
        cin.get();
        cin.get();
        ResultScreen(stage);
    }

    else if(answer == 5 || answer == 6 || answer <= 0 || answer > 9)
    {
        cout << endl << "Podales odpowiedz z poza zakresu!" << endl;
        goto error;
    }
    else if(phone == 0 && answer == 7)
    {
        cout << endl << "Podales odpowiedz z poza zakresu!" << endl;
        goto error;
    }
    else if(half == 0 && answer == 8)
    {
        cout << endl << "Podales odpowiedz z poza zakresu!" << endl;
        goto error;
    }

    else if(audience == 0 && answer == 9)
    {
        cout << endl << "Podales odpowiedz z poza zakresu!" << endl;
        goto error;
    }
}

int main()
{
    vector<QuestionData>vTab;
    vector<HalfStr>vHalf;
    int stage = 1;
    int phone = 1, half = 1, audience = 1;
    Intro();
    int random = RandomQuestion(vTab, stage);
    TheGame(vTab, random, stage, phone, half, audience, vHalf);

    return 0;
}
