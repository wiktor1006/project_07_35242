# MILIONERZY - *wersja bez reklam #

## Instrukcja obsługi ##

Po uruchomieniu program wyświetla ekran startowy, który wymaga od użytkownika potwierdzenia chęci dalszego korzystania z programu.
Gra uruchamia się bezpośrednio po wciśnięciu przez użytkownika klawisza Enter.

### Etapy gry ###

W grze użytkowik przechodzi przez 15 etapów o zróżnicowanej trudności pytań. Użytkownik otrzymuje jedno pytanie, które jest losowane spośród 10 pytań przewidziany na każdy etap gry. Gracz ma możliwość skorzystania z trzech kół ratunkowych tj. pytanie do publiczności, odrzucenie dwóch niepoprawych odpowiedzi (50/50) oraz telefon do przyjaciela.
Po wprowadzeniu poprawnej odpowiedzi użytkownik przechodzi do następnego etapu. W przypadku podania niepoprawnej odpowiedzi program kończy działanie z informacją o uzyskanym etapie gry.

### Działanie kół ratunkowych ###

W zależności od etapu gry, program decyduje czy użytkownik otrzyma poprawną lub losową odpowiedź. Losowa odpowiedź nie musi być poprawna.

* telefon do przyjaciela – gracz otrzymuje sugerowaną odpowiedź, nie musi być poprawna,
* 50/50 – program usuwa dwie niepoprawne odpowiedzi. Na ekranie wyświetlają się pozostałe odpowiedzi,
* pytanie do publiczności – program wyświetla procentowy udział odpowiedzi udzielnych przez publiczność.

## Testowanie kodu oraz debugowanie ##

Podczas tworzenia kodu, w celu potwierdzenia poprawnego działania funkcji oraz poprawnego uzyskania oczekiwanych wartości zmiennych wyświetlaliśmy za pomocą wydrukunwndanych częściach kodu komunikaty testujące, które drukowały "test" lub wartości zmiennych. Dzięki tej metodzie mogliśmy w szybki sposób zweryfikować postępy naszej pracy oraz naprawić występujące błędy.

## Podział pracy ##

* Igor Tokarski (35239): przygotowanie podstawowej funkcji gry, przygotowanie kół ratunkowych, debugowanie
* Wiktor Uszko (35242): koordynacja projektu, przygotowanie plików .csv, przygotowanie funkcji wczytującej, testowanie programu

## Oprogramowanie ##

* QT Creator, 
* system kontroli GIT, 
* platforma Bitbucket.